<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<!DOCTYPE html>
<f:view>
<html>
<head>
<meta charset="utf-8">
<title>Login</title>
<script type="text/javascript">
function pobierzSugestie() {
	pole = document.getElementById("pole").value;
	if (pole != "") {
		requester = new XMLHttpRequest();
		requester.onreadystatechange = myHandler;
		requester.open("POST", "sugestie");
		requester.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
		requester.send("query=" + pole);
	}
}

function myHandler() {
	if(requester.readyState == 4) {
		if(requester.status == 200) {
			//request.responseText;

			wynik = requester.responseText.split(";");
			el = document.getElementById("lista");
			rezultat = "";
			if(wynik.length > 0) {
				for(i = 0; i < wynik.length; ++i) { 
					console.log(wynik[i])
					if(wynik[i] != "") rezultat +="<span onclick=\"getElementById('pole').value = '" + wynik[i] +"'\">" +  wynik[i] + "</span></br>"
				}
			}
			el.innerHTML = rezultat;
		//	greeting = requester
		//	.responseXML
		//	.getElementByTagName("powitanie")[0]
	//		.firstChild.nodeValue
		}
	}
}
</script>
</head>
<body>
Zaloguj:

<h:form>
<p>Login: <h:inputText id="username" 
	value="#{user.login}"
	required="true"
	label="uzytkownik"
	>
		
	</h:inputText> 
</p>

<p>Hasło:
	<h:inputSecret id="password" 
		value="#{user.haslo}"
		required="true"
		label="haslo"
		>
	</h:inputSecret>
</p>

<p>
<h:commandButton
		value="Zaloguj" 
		id="submitButton" 
		action="#{user.zaloguj}"
		/>
</p>
</h:form>
<h4>Podaj imię</h4>
<input id="pole" type="text" onkeyup="javascript:pobierzSugestie()" />
<div id="hello"></div>
<ul id="lista"></ul>
</body>
</html>
</f:view>