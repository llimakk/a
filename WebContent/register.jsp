<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<!DOCTYPE html>
<f:view>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
</head>
<body>
Zarejestruj:

<h:form>
<p>Login: <h:inputText id="login" 
	value="#{user.login}"
	required="true"
	label="uzytkownik"
	validator="#{user.validateLogin}"
	>
	</h:inputText> 
	<h:message for="login" />
</p>

<p>Hasło:
	<h:inputSecret id="password" 
		value="#{user.haslo}"
		required="true"
		label="haslo"
		>
		<f:validator validatorId="KSValidator"/>
	</h:inputSecret>
	<h:message for="password" />
</p>

<p>
<h:commandButton
		value="Zarejestruj" 
		id="submitButton" 
		action="#{user.register}"
		/>
</p>
</h:form>

</body>
</html>
</f:view>