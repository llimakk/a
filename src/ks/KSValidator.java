package ks;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.EmailValidator;

public class KSValidator implements Validator{

	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
		HtmlInputSecret htmlInputSecret = (HtmlInputSecret) uiComponent;
		
		if(!StringUtils.isAlphanumeric((String)value)) {
			FacesMessage facesMessage = new FacesMessage(htmlInputSecret.getLabel() + ": Dozwolne s� jedynie znaki alfanumeryczne.");
			throw new ValidatorException(facesMessage);
		}
	}
}