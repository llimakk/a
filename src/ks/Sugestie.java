package ks;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Servlet implementation class Sugestie
 */
@WebServlet("/sugestie")
public class Sugestie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	String databaseUrl = "jdbc:sqlite:bazadanych.sqlite";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sugestie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain:charset=utf-8");
		response.setCharacterEncoding("utf-8");
		
		String query = request.getParameter("query");
		if(query == null) {
			response.getWriter().append("").close();
			return;
		}
		
		String wynik = "";
		
		try {
			ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
			Dao<KSImieniny, Integer> imieninyDao = DaoManager.createDao(connectionSource, KSImieniny.class);
			
			//TableUtils.dropTable(connectionSource, KSImieniny.class, true);
			TableUtils.createTableIfNotExists(connectionSource, KSImieniny.class);
			
			List<KSImieniny> imieninyAll = imieninyDao.queryForAll();
			System.out.println(imieninyAll.size());
			if(imieninyAll.size() == 0) {
				KSImieniny imieniny1 = new KSImieniny("Kamil", 1, 1);
				KSImieniny imieniny2 = new KSImieniny("Krystian", 2, 2);
				KSImieniny imieniny3 = new KSImieniny("Kamila", 3, 4);
				KSImieniny imieniny4 = new KSImieniny("Agnieszka", 4, 5);
				KSImieniny imieniny5 = new KSImieniny("Adrain", 5, 6);
				KSImieniny imieniny6 = new KSImieniny("Krysia", 6, 7);
				imieninyDao.create(imieniny1);
				imieninyDao.create(imieniny2);
				imieninyDao.create(imieniny3);
				imieninyDao.create(imieniny4);
				imieninyDao.create(imieniny5);
				imieninyDao.create(imieniny6);
			}
			
			GenericRawResults<String[]> rawResults = imieninyDao.queryRaw("SELECT * FROM IMIENINY WHERE IMIE LIKE \"" + query + "%\"");
			List<String[]> dbWynik = rawResults.getResults();
			
			for(String[] tmp : dbWynik) {
				KSImieniny current = new KSImieniny(tmp[1], Integer.parseInt(tmp[2]), Integer.parseInt(tmp[3]));
				wynik += current.getMonthDay() + ";";
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		PrintWriter out = response.getWriter();
		out.append(wynik);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
