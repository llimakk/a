package ks;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName =  "IMIENINY")
public class KSImieniny {

	@DatabaseField(generatedId = true)
	 private int id;
	
	@DatabaseField(columnName = "IMIE")
	 private String imie;
	
	@DatabaseField(columnName = "MIESIAC")
	 private int miesiac;
	
	@DatabaseField(columnName = "DZIEN")
	 private int dzien;
	
	public String getMonthDay() {
		return String.valueOf(this.miesiac) + "-" + String.valueOf(this.dzien);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public int getMiesiac() {
		return miesiac;
	}

	public void setMiesiac(int miesiac) {
		this.miesiac = miesiac;
	}

	public int getDzien() {
		return dzien;
	}

	public void setDzien(int dzien) {
		this.dzien = dzien;
	}

	public KSImieniny(String imie, int miesiac, int dzien) {
		super();
		this.imie = imie;
		this.miesiac = miesiac;
		this.dzien = dzien;
	}

	public KSImieniny() {
		super();
	}
	
	
}
