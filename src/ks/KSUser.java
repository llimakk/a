package ks;


import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.EmailValidator;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.table.TableUtils;

@DatabaseTable(tableName =  "USERS")
public class KSUser {
	String databaseUrl = "jdbc:sqlite:bazadanych.sqlite";
	
	@DatabaseField(generatedId = true)
	 private int id;
	
	@DatabaseField(columnName = "LOGIN")
	 private String login;
	
	@DatabaseField(columnName = "HASLO")
	 private String haslo;

	public KSUser(String login, String haslo) {
		super();
		this.login = login;
		this.haslo = haslo;
	}
	
	public KSUser() {
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}
	
	public String register() {
		try {
			ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
			Dao<KSUser, Integer> userDao = DaoManager.createDao(connectionSource, KSUser.class);
			
			TableUtils.createTableIfNotExists(connectionSource, KSUser.class);
			
			userDao.create(this);
			
			connectionSource.close();
			return "RegisterSuccess";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "RegisterError";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "RegisterError";
		}
	}
	
	public String zaloguj() {

		try {
			ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);
			Dao<KSUser, Integer> userDao = DaoManager.createDao(connectionSource, KSUser.class);
			
			TableUtils.createTableIfNotExists(connectionSource, KSUser.class);
			
			Map<String, Object> credentials = new HashMap<String, Object>();
			credentials.put("LOGIN", this.login);
			credentials.put("HASLO", this.haslo);
			List<KSUser> results = userDao.queryForFieldValues(credentials);
			
			String message = "LoginError";
			if(results.size() == 1) {
				message = "LoginSuccess";
			}
			connectionSource.close();
			return message;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "LoginError";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "LoginError";
		}
	}

	public void validateLogin(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException{
		EmailValidator emailValidator = EmailValidator.getInstance();
		
		if(!emailValidator.isValid((String) value)) {
			FacesMessage facesMessage = new FacesMessage("Login musi by� adresem email");
			
			throw new ValidatorException(facesMessage);
		}
	}
}
